from celery import shared_task


@shared_task
def sum_number(*args):
    return sum(args)
